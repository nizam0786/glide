@extends('layouts.app')

@section('content')
    <div class="px-10 md:px-40 py-10">
        <div class="flex justify-between items-end text-indigo-700">
            <span class="text-4xl ">Glide</span>
            <span class="text-lg">{{ \Carbon\Carbon::today()->toFormattedDateString() }}</span>
        </div>
        <div class="flex flex-wrap justify-between bg-white rounded-lg p-4 w-100 mt-5">
            <form method="post" action="/calorific/import" enctype="multipart/form-data"
                  class="flex flex-col space-y-5">
                @csrf
                <input type="file" name="file">

                <button type="submit"
                        class="w-32 bg-indigo-700 hover:bg-indigo-800 text-white font-bold py-1 px-4 rounded">
                    Import Data
                </button>

                @if ($errors->has('file'))
                    <div class="text-red-700 items-center">
                        {{ $errors->first('file') }}
                    </div>
                @endif
            </form>

            <div class="">
                <!-- small graphs/charts or filters can go here -->
            </div>
        </div>

        <!-- data table -->
        <div class="flex flex-col mt-10">
            <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
                <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                    <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
                        <table class="min-w-full divide-y divide-gray-200">
                            <thead class="bg-gray-50">
                            <tr>
                                <th scope="col"
                                    class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                    Applicable at
                                </th>
                                <th scope="col"
                                    class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                    Applicable for
                                </th>
                                <th scope="col"
                                    class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                    Data Item
                                </th>
                                <th scope="col"
                                    class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                    Value
                                </th>
                                <th scope="col"
                                    class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                    Generated time
                                </th>
                                <th scope="col"
                                    class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                    Quality indicator
                                </th>
                                <th scope="col" class="relative px-6 py-3">
                                    <span class="sr-only">Edit</span>
                                </th>
                            </tr>
                            </thead>
                            <tbody class="bg-white divide-y divide-gray-200">
                            @foreach($calorificItems as $item)
                                <tr>
                                    <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                                        {{ $item->applicable_at->format('d/m/Y H:i:s') }}
                                    </td>

                                    <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                                        {{ $item->applicable_for->format('d/m/Y') }}
                                    </td>

                                    <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                                        {{ $item->data_type . ', ' . $item->area->name}}
                                    </td>

                                    <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                                        {{ $item->value }}
                                    </td>

                                    <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                                        {{ $item->generated_time->format('d/m/Y H:i:s') }}
                                    </td>

                                    <td class="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                                        {{ $item->quality_indicator }}
                                    </td>

                                    <td class="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
                                        <a href="#" class="text-indigo-600 hover:text-indigo-900">Edit</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="my-10">
            {{ $calorificItems->links() }}
        </div>
    </div>
@endsection
<?php

namespace App\Imports;

use App\Models\Area;
use App\Models\Calorific;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Row;
use Maatwebsite\Excel\Concerns\OnEachRow;


class CalorificsImport implements OnEachRow
{

    public function onRow(Row $row)
    {
        // Get the index of the row and the data.
        $rowIndex = $row->getIndex();
        $row  = $row->toArray();

        // We are skipping the first row which contains the headings and we are formatting
        // the dates to the correct format so that we can store it in the database.
        if($rowIndex > 1){
            $applicableAt = Carbon::createFromFormat('d/m/Y H:i:s', $row[0])->toDateTimeString();
            $applicableFor = Carbon::createFromFormat('d/m/Y', $row[1])->toDateString();
            $generatedTime = Carbon::createFromFormat('d/m/Y H:i:s', $row[4])->toDateTimeString();
            $data = explode(',', $row[2]);
            $dataType = $data[0];
            $area = $data[1];

            /// / create models from the row of data which we have gathered from the csv.
            $area = Area::firstOrCreate([
                'name' => $area,
            ]);

            // create models from the row of data which we have gathered from the csv.
            $calorific = Calorific::firstOrCreate([
                'applicable_at' => $applicableAt,
                'applicable_for' =>  $applicableFor,
                'area_id' => $area->id,
                'data_type' => $dataType,
                'value' => $row[3],
                'generated_time' => $generatedTime,
                'quality_indicator' => $row[5],
            ]);
        }
    }

    public function chunkSize(): int
    {
        return 1000;
    }
}

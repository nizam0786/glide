<?php

namespace App\Http\Controllers;

use App\Http\Requests\ImportCalorificRequest;
use App\Imports\CalorificsImport;
use App\Models\Area;
use App\Models\Calorific;
use Illuminate\Http\Request;
use Excel;

class CalorificController extends Controller
{
    public function index(){
        $calorificItems = Calorific::simplePaginate(60);

        return view('calorific.index', compact('calorificItems'));
    }

    public function import(ImportCalorificRequest $request)
    {
        Excel::import(new CalorificsImport(), $request->file('file')->store('temp'));
        return back();
    }
}

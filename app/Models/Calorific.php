<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Calorific extends Model
{
    use HasFactory;

    protected $fillable = [
        'applicable_at',
        'applicable_for',
        'data_type',
        'area_id',
        'value',
        'generated_time',
        'quality_indicator'
    ];

    protected $dates = [
        'applicable_at',
        'applicable_for',
        'generated_time',
    ];

    public function area()
    {
        return $this->belongsTo(Area::class);
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCalorificsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('calorifics', function (Blueprint $table) {
            $table->id();
            $table->dateTime('applicable_at');
            $table->date('applicable_for');
            $table->string('data_type');
            $table->foreignId('area_id')->constrained('areas');
            $table->float('value');
            $table->dateTime('generated_time');
            $table->string('quality_indicator')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('calorifics');
    }
}
